
# Smelly Machine

Welcome to the **Smelly Machine** project, a showcase website built using the **Statiq** Python static site generator framework. Dive in to experience the simplicity, flexibility, and power of Statiq.

[![Built with Statiq](https://img.shields.io/badge/Built%20with-Statiq-blue)](https://github.com/pystatiq/statiq)

## Why Statiq?

### Extendability
Statiq is built with extensibility in mind. With a modular architecture, developers can easily add new features, plugins, and integrations to meet the specific needs of any project.

### Simplicity
No more struggling with complex configurations. Statiq offers a straightforward approach to static site generation, allowing you to focus on your content and design.

### Cloud-Agnostic Deployment
With Statiq, you're not tied to a single cloud provider. You can deploy your site to any cloud provider of your choice with minimal setup.

### Cost-Effective Hosting
Static sites are inherently cheap to host. With Statiq, not only do you benefit from the performance advantages of static sites, but you also save money on hosting costs.

### Easy Deployment to Cloudflare Pages
We understand the importance of seamless deployment. That's why we've included a `pages.sh` script, making it a breeze to deploy your site to Cloudflare Pages.

## Getting Started

1. Clone the **Smelly Machine** repository:
```bash
git clone https://gitlab.com/folxit-public/smelly-statiq.git
```

2. Navigate to the project directory:
```bash
cd smelly-statiq
```

3. Install the necessary dependencies:
```bash
pip install -r requirements.txt
```

4. Build and serve the site locally using Statiq:
```bash
statiq build

python3 -m http.server -d build 8000
```

5. Visit `http://localhost:8000` in your browser to view the site.

## Deployment to Cloudflare Pages

Using the provided `pages.sh` script, deployment to Cloudflare Pages is a cinch:

```bash
./pages.sh
```

You can see working site here: 

https://smelly-statiq.pages.dev

Follow the on-screen instructions, and your site will be live on Cloudflare Pages in no time!


## Notice

### Ownership of Data

All data and related content used in this repository belong to **Reckitt**. Any references, uses, or depictions of said data are strictly for illustrative and demonstration purposes. 

### Technology Demonstrator

This repository serves as a technology demonstrator, showcasing the capabilities of the **Statiq** Python static site generator framework and its potential applications. It is not intended for commercial use, redistribution, or any other purposes beyond demonstration.

### No Endorsement

The use of Reckitt's data does not imply any endorsement, partnership, or affiliation with Reckitt. All representations are purely for the sake of demonstration and should not be construed as official representations of Reckitt or its associated brands.

### Usage Restrictions

Users and viewers of this repository are reminded to respect the rights and ownership of Reckitt concerning the data. Unauthorized use, redistribution, or reproduction of the data outside of this demonstration is strictly prohibited.




---

Crafted with ❤️ using [Statiq](https://github.com/pystatiq/statiq).
