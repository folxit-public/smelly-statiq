document.addEventListener("DOMContentLoaded", function () {
    let lazyElements = [].slice.call(document.querySelectorAll("img.img-lazy, div.img-lazy"));

    if ("IntersectionObserver" in window) {
        let lazyObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    let lazyElement = entry.target;

                    if (lazyElement.tagName === "IMG") {
                        lazyElement.src = lazyElement.dataset.src;
                        if (lazyElement.dataset.retinaSrc) {
                            lazyElement.srcset = `${lazyElement.dataset.retinaSrc} 2x`;
                        }
                    } else if (lazyElement.tagName === "DIV" && lazyElement.dataset.src) {
                        lazyElement.style.backgroundImage = `url("${lazyElement.dataset.src}")`;
                    }

                    // Remove the img-lazy class
                    lazyElement.classList.remove("img-lazy");

                    // Unobserve the element after we've loaded the actual content
                    lazyObserver.unobserve(lazyElement);
                }
            });
        });

        lazyElements.forEach(function (lazyElement) {
            lazyObserver.observe(lazyElement);
        });
    } else {
        // Fallback for browsers that don't support IntersectionObserver
        lazyElements.forEach(function (lazyElement) {
            if (lazyElement.tagName === "IMG") {
                lazyElement.src = lazyElement.dataset.src;
                if (lazyElement.dataset.retinaSrc) {
                    lazyElement.srcset = `${lazyElement.dataset.retinaSrc} 2x`;
                }
            } else if (lazyElement.tagName === "DIV" && lazyElement.dataset.src) {
                lazyElement.style.backgroundImage = `url("${lazyElement.dataset.src}")`;
            }

            lazyElement.classList.remove("img-lazy");
        });
    }
});
