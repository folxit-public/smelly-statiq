<span class="generic-style-8">If you live in a <u>[**hard water area**](https://www.calgon.co.uk/3in1/hard-water-area-map/)</u>, 
you may notice you have a smelly washing machine. <u>[**Limescale**](https://www.calgon.co.uk/am-i-affected-by-hard-water/what-is-hard-water-and-limescale/)</u> 
can cause blockages throughout your washing machine, and a large amount of soap scum may build up as a result of using excessive amounts of laundry detergent to combat the effects of hard water. 
This can allow <u>[**dirt or other residue**](https://www.calgon.co.uk/am-i-affected-by-hard-water/how-does-dirt-residue-impacts-my-washing-machine-and-laundry/)</u> that may enter your washing machine to accumulate, most frequently in the drum, the door, or around the rubber seal in the rim. The presence of soap scum causes the formation of ‘3-methyl-butanal’, which is responsible for the cheesy, sweaty smell in your washing machine.</span>

<br />

<span class="generic-style-8">Having a washing machine filled with growing bacteria can also have consequences on your laundry. If your washing machine smells of mould, then it’s likely that your clothing will, too. Not only is smelly washing unpleasant, but it is also a hygiene concern since the ‘clean’ clothes you’re wearing are full of the bacteria that has been developing in your washing machine.</span>

8<

<span class="generic-style-8">While you may find temporary solutions to reduce the bad odour from your washing machine or to make your laundry smell better, the best measure would be a preventative one. Calgon stops the creation of limescale and invalidates the use of excessive detergents, preventing the accumulation of any soap scum, dirt or residue, and keeping your washing machine clear of bacteria build-up and malodours. With Calgon, you can place your trust in a healthy, sparkling clean and odour-free washing machine, and feel confident in the fresh clothes that you and your family are putting on each morning. You can pick which Calgon format to use in your wash: <u>[**Gel**](https://www.calgon.co.uk/products/calgon-3in1-gel/calgon-3in1-gel/)</u>, <u>[**Tablets**](https://www.calgon.co.uk/products/calgon-3in1-tablets-30-tabs/calgon-3in1-tablets-30-tabs/)</u>, or [**Powder**](https://www.calgon.co.uk/products/calgon-3in1-powder/calgon-3in1-powder/)</u>.</span>

