from statiq.page import Page
import markdown
import json

md_converter = markdown.Markdown(output_format="html5")


def get_page_data():
    with open("data/hero.md", "r") as f:
        hero = [md_converter.convert(md) for md in f.read().split("\n8<\n")]

    with open("data/products.json") as f:
        products = json.loads(f.read())

    return {"hero_html": hero, "products": products}


head = [
    # you can use a string to put content in the head
    "<title>Smelly Washing Machine?</title>",
    '<meta name="robots" content="noindex">',
    # or you can use a dictionary to create an element
    {
        "tag": "meta",
        "attributes": {
            "content": "width=device-width, initial-scale=1.0",
            "name": "viewport",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "telephone=no",
            "name": "format-detection",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "IE=edge,chrome=1",
            "http-equiv": "X-UA-Compatible",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "text/html; charset=utf-8",
            "http-equiv": "content-type",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "Are you wondering why your washing machine smells? Bad odours can be caused by limescale, find out how you can clean your washing machine with Calgon.",
            "name": "description",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "Smelly Washing Machine, Washing machine stinks, how to clean washing machine, smelly washer cleaner, why does my washer smell",
            "name": "keywords",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "noindex,nofollow",
            "name": "robots",
        },
    },
    {
        "tag": "link",
        "attributes": {
            "href": "https://promo.calgon.co.uk/articles/smelly-washing-machine/",
            "rel": "canonical",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "en_US",
            "property": "og:locale",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "article",
            "property": "og:type",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "",
            "property": "og:title",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "",
            "property": "og:description",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "",
            "property": "og:site_name",
        },
    },
    {
        "tag": "meta",
        "attributes": {
            "content": "https://promo.calgon.co.uk/articles/smelly-washing-machine/",
            "property": "og:url",
        },
    },
    {
        "tag": "link",
        "attributes": {
            "crossorigin": "",
            "href": "https://fonts.gstatic.com",
            "rel": "preconnect dns-prefetch",
        },
    },
    # you can even load scripts to <head> from file:
    open("data/gtm.txt").read(),
    open("data/styles.txt").read(),
    open("data/head_scripts.txt").read(),
]


def page():
    return Page(
        data=get_page_data,
        head=head,
    )
